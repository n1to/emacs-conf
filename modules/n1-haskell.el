;;; n1-haskell.el --- Haskell Language Support  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  n1to

;; Author: n1to

;;; Commentary:

;; This module provides support for all kinds of Haskell development.

;;; Code:

(n1-package-install-package 'haskell-mode)
(n1-package-install-package 'haskell-snippets)
;; (n1-package-install-package 'flymake-hlint)

(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)

(with-eval-after-load "haskell-mode"
  (define-key haskell-mode-map (kbd "C-c e n") #'flymake-goto-next-error)
  (define-key haskell-mode-map (kbd "C-c e p") #'flymake-goto-prev-error))

;; hslint on the command line only likes this indentation mode;
;; alternatives commented out below.
;; (add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
;;(add-hook 'haskell-mode-hook 'turn-on-haskell-indent)
;;(add-hook 'haskell-mode-hook 'turn-on-haskell-simple-indent)

;; Ignore compiled Haskell files in filename completions
(add-to-list 'completion-ignored-extensions ".hi")

;; (add-hook 'haskell-mode-hook 'flymake-hlint-load)

;; add ghcup paths
(add-to-list 'exec-path "~/.ghcup/bin")
(add-to-list 'exec-path "~/.cabal/bin")

(provide 'n1-haskell)
;;; n1-haskell.el ends here
