;;; init.el -*- lexical-binding: t; -*-

;; Profile emacs startup
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs loaded in %s." (emacs-init-time))))

;; Initialize package system (defined in n1-package.el, which is loaded at the
;; end of early-init.el)
(n1-package-initialize)

;; Set default coding system (especially for Windows)
(set-default-coding-systems 'utf-8)
(customize-set-variable 'visible-bell 1)  ; turn off beeps, make them flash!
(customize-set-variable 'large-file-warning-threshold 100000000) ;; change to ~100 MB

;; Check the system used
(defconst ON-LINUX   (eq system-type 'gnu/linux))
(defconst ON-MAC     (eq system-type 'darwin))
(defconst ON-BSD     (or ON-MAC (eq system-type 'berkeley-unix)))
(defconst ON-WINDOWS (memq system-type '(cygwin windows-nt ms-dos)))

;; Load the file of the Customization UI?
(defvar n1-load-custom-file nil)

;; When writing modules, insert header from skeleton
(auto-insert-mode)
(with-eval-after-load "autoinsert"
  ;; Handle a missing `custom-file' by not running `auto-insert' when
  ;; it gets created.  The value of the `custom-file' for Crafted
  ;; Emacs is `custom.el', however, the user could change that to
  ;; something else.  On startup, asking the user to automatically
  ;; insert the standard headers may cause confusion if they choose to
  ;; answer 'y'.  Here we advise the `auto-insert' function to not run
  ;; when the file is the `custom-file' and it is being created.
  (defun ignore-auto-insert-for-custom (orig-auto-insert &rest args)
    "Apply ORIG-AUTO-INSERT only when the file is not the
`custom-file' to avoid confusion when that file doesn't exist on
startup."
    (if (string-match (file-name-nondirectory custom-file) buffer-file-name)
        (message "Skipping auto-insert for %s" custom-file)
      (apply orig-auto-insert args)))
  (advice-add 'auto-insert :around #'ignore-auto-insert-for-custom)
  (define-auto-insert
    (cons (concat (expand-file-name user-emacs-directory) "modules/n1-.*\\.el")
          "Emacs Lisp Skeleton")
    '("Emacs Module Description: "
      ";;;; " (file-name-nondirectory (buffer-file-name)) " --- " str
      (make-string (max 2 (- 80 (current-column) 27)) ?\s)
      "-*- lexical-binding: t; -*-" '(setq lexical-binding t)
      "

;; Copyright (C) " (format-time-string "%Y") "
;; SPDX-License-Identifier: MIT

;; Author: n1to

;;; Commentary:

;; " _ "

;;; Code:

(provide '"
      (file-name-base (buffer-file-name))
      ")
;;; " (file-name-nondirectory (buffer-file-name)) " ends here\n")))


;; Load configuration in config.el, which contains the rest of the configuration
;; and which modules are to be loaded.
(load (expand-file-name "n1-config.el" user-emacs-directory))

;; The file used by the Customization UI to store value-setting forms in a
;; customization file, rather than at the end of the `init.el' file, is called
;; `custom.el'. The file is loaded after this `init.el' file Any variable values
;; set in the user config will be overridden with the values set with the
;; Customization UI and saved in the custom file.
(customize-set-variable 'custom-file
  (expand-file-name "custom.el" user-emacs-directory))

;; The custom file will only be loaded if `n1-load-custom-file' is set.
(when n1-load-custom-file
  (load custom-file t))

;; Make GC pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000))

;;; init.el ends here
