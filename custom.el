(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-fold-core-style 'overlays)
 '(package-selected-packages
   '(yasnippet-snippets which-key web-mode vertico sly-repl-ansi-color sly-quicklisp sly-asdf raku-mode rainbow-delimiters racket-mode pkg-info password-store paredit org-appear orderless marginalia magit lin ligature hyperbole helpful haskell-snippets haskell-mode flymake-rakudo embark-consult elisp-demos eldoc ef-themes denote csv-mode corfu-terminal cider cape beancount all-the-icons aggressive-indent)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
