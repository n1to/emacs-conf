;;; n1-perl.el --- Crafted Perl Language Support  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  n1to

;; Author: n1to

;;; Commentary:

;; This module provides improved support for the perl programming language.

;;; Code:

(require 'cperl-mode)
(defalias 'perl-mode 'cperl-mode)
(setq cperl-indent-level 2
      cperl-close-paren-offset -2
      cperl-continued-statement-offset 2
      cperl-indent-parens-as-block t
      cperl-tab-always-indent t)

(add-to-list 'auto-mode-alist '("\\.t\\'" . cperl-mode))

(provide 'n1-perl)
;;; n1-perl.el ends here
