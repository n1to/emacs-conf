;;;; n1-ocaml.el --- OCaml Language Support          -*- lexical-binding: t; -*-

;; Copyright (C) 2024
;; SPDX-License-Identifier: MIT

;; Author: n1to

;;; Commentary:

;; This module sets up Emacs for OCaml development. For this configuration
;; to work the OCaml package manager `opam' needs to be installed and setup.
;; To install the Emacs modules, run:
;;
;;   opam install odoc ocamlformat utop tuareg merlin

;;; Code:

(defvar n1-opam-bin-directory (ignore-errors (car (process-lines "opam" "var" "bin"))))
(defvar n1-opam-share-directory (ignore-errors (car (process-lines "opam" "var" "share"))))

(when (and n1-opam-bin-directory (file-directory-p n1-opam-bin-directory))
  (add-to-list 'exec-path n1-opam-bin-directory))

(when (and n1-opam-share-directory (file-directory-p n1-opam-share-directory))
  (add-to-list 'load-path (expand-file-name "emacs/site-lisp" n1-opam-share-directory))
  (require 'ocamlformat)
  (require 'tuareg)
  (require 'dune)
  (require 'dune-watch)
  
  (autoload 'utop "utop" "Toplevel for OCaml" t)
  (setq utop-command "opam exec -- utop -emacs")
  (autoload 'utop-minor-mode "utop" "Minor mode for utop" t)
  (add-hook 'tuareg-mode-hook 'utop-minor-mode)
  
  (add-to-list 'auto-mode-alist '("\\.ml[iylp]?$" . tuareg-mode))

  (let ((path (concat "PATH="
		      (seq-reduce
		       (lambda (acc x)
			 (concat acc ":" x))
		       (cdr exec-path) (car exec-path)))))
    (defun n1-merlin-config ()
      `((name . "n1merlin")
	(env . (,path)))))

  (autoload 'merlin-mode "merlin" nil t nil)
  (setq merlin-configuration-function 'n1-merlin-config)
  (add-hook 'tuareg-mode-hook 'merlin-mode t)
  (add-hook 'tuareg-mode-hook (lambda ()
				(define-key tuareg-mode-map (kbd "C-M-<tab>") #'ocamlformat)
				(add-hook 'before-save-hook #'ocamlformat-before-save))))

;; TODO: checkout these in the same folder as above
;; dune-flymake.el

(provide 'n1-ocaml)
;;; n1-ocaml.el ends here
