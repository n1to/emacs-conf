;;; n1-package.el --- Configures the package system  -*- lexical-binding: t; -*-

;; Copyright (C) 2023

;; Author:  <n1to>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'package)
(require 'time-date)

(defvar n1-package-perform-stale-archive-check t
 "Flag to allow the user to turn off checking if an archive is stale on startup.")

(defvar n1-package-update-days 1
  "The number of days old a package archive must be before it is considered stale.")

;; Emacs 27.x has gnu elpa as the default
;; Emacs 28.x adds the nongnu elpa to the list by default, so only
;; need to add nongnu when this isn't Emacs 28+
(when (version< emacs-version "28")
  (add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/")))
(add-to-list 'package-archives '("stable" . "https://stable.melpa.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

(customize-set-variable 'package-archive-priorities
                        '(("gnu"    . 99)   ; prefer GNU packages
                          ("nongnu" . 80)   ; use non-gnu packages if
                                            ; not found in GNU elpa
                          ("stable" . 70)   ; prefer "released" versions
                                            ; from melpa
                          ("melpa"  . 0)))  ; if all else fails, get it
                                            ; from melpa

(customize-set-variable 'package-user-dir (expand-file-name "elpa" user-emacs-directory))
;; make sure the elpa/ folder exists after setting it above.
(unless (file-directory-p package-user-dir)
  (mkdir package-user-dir t))

(defun n1-package-archive-stale-p (archive)
  "Return t if ARCHIVE is stale.

ARCHIVE is stale if the on-disk cache is older than `n1-package-update-days'
old.  If `n1-package-perform-stale-archive-check' is nil, the check is skipped."
  (let* ((today (decode-time nil nil t))
         (archive-name (expand-file-name
                        (format "archives/%s/archive-contents" archive)
                        package-user-dir))
         (last-update-time (decode-time (file-attribute-modification-time
                                         (file-attributes archive-name))))
         (delta (make-decoded-time :day n1-package-update-days)))
    (if n1-package-perform-stale-archive-check
        (time-less-p (encode-time (decoded-time-add last-update-time delta))
                     (encode-time today))
      nil)))

(defun n1-package-archives-stale-p ()
  "Return t if any PACKAGE-ARCHIVES cache is out of date.

Check each archive listed in PACKAGE-ARCHIVES, if the on-disk cache is older
than 1 day, return a non-nil value.  Fails fast, will return t for the first
stale archive found or nil if they are all up-to-date."
  (interactive)
  (cl-some #'n1-package-archive-stale-p (mapcar #'car package-archives)))

(defmacro n1-package-install-package (package)
  "Only install PACKAGE if it is not already installed."
  `(unless (package-installed-p ,package) (package-install ,package)))

(defun n1-package-initialize ()
  "Initialize the package system."

  ;; Only use package.el if it is enabled. The user may have turned it
  ;; off in their `early-config.el' file, so respect their wishes if so.
  (when package-enable-at-startup
    (package-initialize)

    (require 'seq)
    ;; Only refresh package contents once per day on startup, or if the
    ;; `package-archive-contents' has not been initialized. If Emacs has
    ;; been running for a while, user will need to manually run
    ;; `package-refresh-contents' before calling `package-install'.
    (cond ((seq-empty-p package-archive-contents)
           (progn
             (message "n1-init: package archives empty, initializing")
             (package-refresh-contents)))
          ((n1-package-archives-stale-p)
           (progn
             (message "n1-init: package archives stale, refreshing in the background")
             ;; (package-refresh-contents t)
	     )))))

;;; n1-package.el ends here
