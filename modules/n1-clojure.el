;;;; n1-clojure.el --- Clojure Development Modules -*- lexical-binding: t; -*-

;; Copyright (C) 2024
;; SPDX-License-Identifier: MIT

;; Author: n1to

;;; Commentary:

;;

;;; Code:

(n1-package-install-package 'clojure-mode)
(n1-package-install-package 'cider)

(provide 'n1-clojure)
;;; n1-clojure.el ends here
