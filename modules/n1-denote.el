;;; n1-denote.el --- Simple Notes with denote  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  n1to
;; Author: n1to
;; Keywords: convenience, docs, calendar

;;; Commentary:

;;; Code:

(n1-package-install-package 'denote)
(require 'denote)
(require 'org)

(setq n1--denote-otl-front-matter (s-concat denote-text-front-matter "* "))
(add-to-list 'denote-file-types
	     '(outline :extension ".otl"
		       :date-function denote-date-iso-8601
		       :front-matter n1--denote-otl-front-matter
		       :title-key-regexp "^title\\s-*:"
		       :title-value-function denote-format-string-for-org-front-matter
		       :title-value-reverse-function denote-trim-whitespace
		       :keywords-key-regexp "^tags\\s-*:"
		       :keywords-value-function denote-format-keywords-for-text-front-matter
		       :keywords-value-reverse-function denote-extract-keywords-from-front-matter
		       :link denote-org-link-format
		       :link-in-context-regexp denote-org-link-in-context-regexp)
	     t)

;; Remember to check the doc strings of those variables.
(setq denote-directory (expand-file-name "~/Documents/Notes"))
(setq denote-known-keywords '("emacs" "slackware" "nutyx" "awk" "linux" "programming"))
(setq denote-infer-keywords t)
(setq denote-sort-keywords t)
(setq denote-file-type nil) ; Org is the default, set others here
(setq denote-prompts '(title keywords))

;; Pick dates, where relevant, with Org's advanced interface:
(setq denote-date-prompt-use-org-read-date t)

;; We allow multi-word keywords by default.
(setq denote-allow-multi-word-keywords t)

(setq denote-date-format nil) ; read doc string

;; By default, we fontify backlinks in their bespoke buffer.
(setq denote-link-fontify-backlinks t)

(defun n1--denote-mode-hook-init ()
  (set-fill-column 80)
  (auto-fill-mode t))

(add-hook 'denote-mode-hook #'n1--denote-mode-hook-init)
(add-hook 'dired-mode-hook #'denote-dired-mode-in-directories)

(defvar n1-denote-map
  (let ((map (make-sparse-keymap)))
    (define-key map "," 'denote)
    (define-key map "." 'denote-type)
    (define-key map "t" 'denote-template)
    (define-key map "s" 'denote-signature)
    (define-key map "r" 'denote-rename-file)
    (define-key map "a" 'denote-add-links)
    (define-key map "l" 'denote-backlinks)
    map)
  "N1to's global denote keymap.")

(keymap-unset org-mode-map "C-," t)
(keymap-global-set "C-," n1-denote-map)

;; Denote Templates
(setq denote-templates '((todo . "* TODO ")))

(provide 'n1-denote)
;;; n1-denote.el ends here
