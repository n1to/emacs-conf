;;; n1-ui.el --- UI configuration                    -*- lexical-binding: t; -*-

;; Copyright (C) 2023

;; Author:  n1to
;; Keywords: internal

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(n1-package-install-package 'all-the-icons)
(n1-package-install-package 'elisp-demos)
(n1-package-install-package 'helpful)
(n1-package-install-package 'ef-themes)
(n1-package-install-package 'lin)
(n1-package-install-package 'ligature)
(n1-package-install-package 'spacious-padding)

(setopt x-underline-at-descent-line nil)	 ; prettier underlines
(setopt switch-to-buffer-obey-display-actions t) ; more consistent buffer switching
(setopt indicate-buffer-boundaries 'left)        ; show buffer top and bottom on the left margin

;; Modes to highlight the current line with
(let ((hl-line-hooks '(text-mode-hook prog-mode-hook)))
  (mapc (lambda (hook) (add-hook hook 'hl-line-mode)) hl-line-hooks))

;;; Tab-bar Configuration

(setopt tab-bar-show 1)

;; Add the time to the tab-bar, if visible
(add-to-list 'tab-bar-format 'tab-bar-format-align-right 'append)
(add-to-list 'tab-bar-format 'tab-bar-format-global 'append)
(setopt display-time-format "%a %F %T")
(setopt display-time-interval 1)
(display-time-mode)


;;; Face
;; (set-face-attribute 'default nil :family "Fira Code Retina" :height 120)
;; (set-face-attribute 'default nil '(:family "Hack" :height 120)) ; fallback
(set-face-attribute 'default nil :family "Iosevka Fixed" :height 130)
(set-face-attribute 'bold nil :weight 'bold)
(set-face-attribute 'italic nil :weight 'light)

(eval-after-load 'ligature
  (progn
    ;; Enable the "www" ligature in every possible major mode
    (ligature-set-ligatures 't '("www"))
    ;; Enable traditional ligature support in eww-mode, if the
    ;; `variable-pitch' face supports it
    (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
    ;; Enable all Cascadia and Fira Code ligatures in programming modes
    (ligature-set-ligatures 'prog-mode
                            '(;; == === ==== => =| =>>=>=|=>==>> ==< =/=//=// =~
                              ;; =:= =!=
                              ("=" (rx (+ (or ">" "<" "|" "/" "~" ":" "!" "="))))
                              ;; ;; ;;;
                              (";" (rx (+ ";")))
                              ;; && &&&
                              ("&" (rx (+ "&")))
                              ;; !! !!! !. !: !!. != !== !~
                              ("!" (rx (+ (or "=" "!" "\." ":" "~"))))
                              ;; ?? ??? ?:  ?=  ?.
                              ("?" (rx (or ":" "=" "\." (+ "?"))))
                              ;; %% %%%
                              ("%" (rx (+ "%")))
                              ;; |> ||> |||> ||||> |] |} || ||| |-> ||-||
                              ;; |->>-||-<<-| |- |== ||=||
                              ;; |==>>==<<==<=>==//==/=!==:===>
                              ("|" (rx (+ (or ">" "<" "|" "/" ":" "!" "}" "\]"
                                              "-" "=" ))))
                              ;; \\ \\\ \/
                              ("\\" (rx (or "/" (+ "\\"))))
                              ;; ++ +++ ++++ +>
                              ("+" (rx (or ">" (+ "+"))))
                              ;; :: ::: :::: :> :< := :// ::=
                              (":" (rx (or ">" "<" "=" "//" ":=" (+ ":"))))
                              ;; // /// //// /\ /* /> /===:===!=//===>>==>==/
                              ("/" (rx (+ (or ">"  "<" "|" "/" "\\" "\*" ":" "!"
                                              "="))))
                              ;; .. ... .... .= .- .? ..= ..<
                              ("\." (rx (or "=" "-" "\?" "\.=" "\.<" (+ "\."))))
                              ;; -- --- ---- -~ -> ->> -| -|->-->>->--<<-|
                              ("-" (rx (+ (or ">" "<" "|" "~" "-"))))
                              ;; *> */ *)  ** *** ****
                              ("*" (rx (or ">" "/" ")" (+ "*"))))
                              ;; www wwww
                              ("w" (rx (+ "w")))
                              ;; <> <!-- <|> <: <~ <~> <~~ <+ <* <$ </  <+> <*>
                              ;; <$> </> <|  <||  <||| <|||| <- <-| <-<<-|-> <->>
                              ;; <<-> <= <=> <<==<<==>=|=>==/==//=!==:=>
                              ;; << <<< <<<<
                              ("<" (rx (+ (or "\+" "\*" "\$" "<" ">" ":" "~"  "!"
                                              "-"  "/" "|" "="))))
                              ;; >: >- >>- >--|-> >>-|-> >= >== >>== >=|=:=>>
                              ;; >> >>> >>>>
                              (">" (rx (+ (or ">" "<" "|" "/" ":" "=" "-"))))
                              ;; #: #= #! #( #? #[ #{ #_ #_( ## ### #####
                              ("#" (rx (or ":" "=" "!" "(" "\?" "\[" "{" "_(" "_"
					   (+ "#"))))
                              ;; ~~ ~~~ ~=  ~-  ~@ ~> ~~>
                              ("~" (rx (or ">" "=" "-" "@" "~>" (+ "~"))))
                              ;; __ ___ ____ _|_ __|____|_
                              ("_" (rx (+ (or "_" "|"))))
                              ;; Fira code: 0xFF 0x12
                              ("0" (rx (and "x" (+ (in "A-F" "a-f" "0-9")))))
                              ;; Fira code:
                              "Fl"  "Tl"  "fi"  "fj"  "fl"  "ft"
                              ;; The few not covered by the regexps.
                              "{|"  "[|"  "]#"  "(*"  "}#"  "$>"  "^="))
    ;; Enables ligature checks globally in all buffers. You can also do it
    ;; per mode with `ligature-mode'.
    (global-ligature-mode t)))

;; NOTE: modified from 'Sample configuration' by ef-themes manual

(setq ef-themes-headings ; read the manual's entry or the doc string
      '(
	(0 . (variable-pitch light 1.5))
        (1 1.5)
        (2 1.5)
        (3 1.5)
        (4 1.5)
        (5 1.3) ; absence of weight means `bold'
        (6 1.3)
        (7 1.3)
        (t 1.3)
	(agenda-date 1.3)
        (agenda-structure 1.5)))

;; They are nil by default...
(setq ef-themes-mixed-fonts t
      ef-themes-variable-pitch-ui t)

;; Read the doc string or manual for this one.  The symbols can be
;; combined in any order.
(setq ef-themes-region '(intense no-extend))

;; Sacha Chua's todo keyword faces
(defun n1--org-todo-set-keyword-faces ()
  (ef-themes-with-colors
    (setq org-todo-keyword-faces
	  `(("NEXT" . (:foreground ,blue-cooler :weight bold))
	    ("TODO" . (:foreground ,red-warmer :weight bold))
	    ("WAITING" . (:foreground ,red-faint :weight bold))
	    ("SOMEDAY" . (:foreground ,fg-dim :weight bold))
	    ("REPEAT" . (:foreground ,cyan-cooler :weight bold))
	    ("PROJECT" . (:foreground ,magenta :weight bold))
	    ("DONE" . (:foreground ,green-warmer :weight bold))
	    ("CANCELED" . (:foreground ,yellow :weight bold))))
    (when (derived-mode-p 'org-mode)
      (font-lock-fontify-buffer))))
(with-eval-after-load 'ef-themes
  (add-hook 'ef-themes-post-load-hook #'n1--org-todo-set-keyword-faces))

;; Disable all other themes to avoid awkward blending:
(mapc #'disable-theme custom-enabled-themes)
;; Disable all other themes when switching to prevent awkward blending:
(setq ef-themes-disable-other-themes t)
;; load the theme which also calls `ef-themes-post-load-hook':
(ef-themes-select 'ef-frost)
;; If you like two specific themes and want to switch between them, you
;; can specify them in `ef-themes-to-toggle' and then invoke the command
;; `ef-themes-toggle'.  All the themes are included in the variable
;; `ef-themes-collection'.
(setq ef-themes-to-toggle '(ef-frost ef-maris-dark))
;; Keybindings:
(define-key global-map (kbd "<f5>") #'ef-themes-toggle)

;;; Lin Mode

(require 'lin)

(setq lin-face 'lin-blue) ; check doc string for alternative styles
(lin-global-mode 1)

;;; Help Buffers

;; Make `describe-*' screens more helpful
(require 'helpful)
(define-key helpful-mode-map [remap revert-buffer] #'helpful-update)
(global-set-key [remap describe-command] #'helpful-command)
(global-set-key [remap describe-function] #'helpful-callable)
(global-set-key [remap describe-key] #'helpful-key)
(global-set-key [remap describe-symbol] #'helpful-symbol)
(global-set-key [remap describe-variable] #'helpful-variable)
(global-set-key (kbd "C-h F") #'helpful-function)

;; Bind extra `describe-*' commands
(global-set-key (kbd "C-h K") #'describe-keymap)

;;; Line Numbers

(defvar n1-ui-line-numbers-enabled-modes '(conf-mode prog-mode)
  "Modes which should display line numbers.")

(defvar n1-ui-line-numbers-disabled-modes '(org-mode)
  "Modes which should not display line numbers.
Modes derived from the modes defined in
`crafted-ui-line-number-enabled-modes', but should not display line numbers.")

(defun n1-ui--enable-line-numbers-mode ()
  "Turn on line numbers mode.

Used as hook for modes which should display line numbers."
  (display-line-numbers-mode 1))

(defun n1-ui--disable-line-numbers-mode ()
  "Turn off line numbers mode.

Used as hook for modes which should not display line numebrs."
  (display-line-numbers-mode -1))

(defvar n1-ui-display-line-numbers t)

(defun n1-ui-update-line-numbers-display ()
  "Update configuration for line numbers display."
  (if n1-ui-display-line-numbers
      (progn
        (dolist (mode n1-ui-line-numbers-enabled-modes)
          (add-hook (intern (format "%s-hook" mode))
                    #'n1-ui--enable-line-numbers-mode))
        (dolist (mode n1-ui-line-numbers-disabled-modes)
          (add-hook (intern (format "%s-hook" mode))
                    #'n1-ui--disable-line-numbers-mode))
        (setq-default
         display-line-numbers-grow-only t
         display-line-numbers-type t
         display-line-numbers-width 2))
     (progn
       (dolist (mode n1-ui-line-numbers-enabled-modes)
         (remove-hook (intern (format "%s-hook" mode))
                      #'n1-ui--enable-line-numbers-mode))
       (dolist (mode n1-ui-line-numbers-disabled-modes)
         (remove-hook (intern (format "%s-hook" mode))
                      #'n1-ui--disable-line-numbers-mode)))))

(n1-ui-update-line-numbers-display)

;;; Elisp-Demos

;; also add some examples
(require 'elisp-demos)
(advice-add 'helpful-update :after #'elisp-demos-advice-helpful-update)

;; add visual pulse when changing focus, like beacon but built-in
;; from from https://karthinks.com/software/batteries-included-with-emacs/
(defun pulse-line (&rest _)
  "Pulse the current line."
  (pulse-momentary-highlight-one-line (point)))

(dolist (command '(scroll-up-command scroll-down-command
				     recenter-top-bottom other-window))
  (advice-add command :after #'pulse-line))

;;; All The Icons

(when (display-graphic-p)
  (require 'all-the-icons)
  ;; Check if necessary fonts are already installed.  If not install them.
  (unless (find-font (font-spec :name "all-the-icons"))
    (all-the-icons-install-fonts t)))

(when (package-installed-p 'spacious-padding)
  (require 'spacious-padding)

  ;; These are the default values, but I keep them here for visibility.
  (setq spacious-padding-widths
	'( :internal-border-width 15
           :header-line-width 4
           :mode-line-width 6
           :tab-width 4
           :right-divider-width 30
           :scroll-bar-width 8
           :fringe-width 8))

  ;; Read the doc string of `spacious-padding-subtle-mode-line' as it
  ;; is very flexible and provides several examples.
  ;; (setq spacious-padding-subtle-mode-line
  ;; 	`( :mode-line-active 'default
  ;;          :mode-line-inactive vertical-border))

  (spacious-padding-mode 1))

(provide 'n1-ui)
;;; n1-ui.el ends here
