;;; n1-config.el --- N1 Emacs configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2024

;; Author: n1to

;;; Commentary:

;;; Code:

(defvar n1-config-module-directory
  (expand-file-name "modules/" user-emacs-directory)
  "Directory for modules used by N1 Emacs to enable different functionality.")

(defvar n1-config-var-directory
  (expand-file-name "var/" user-emacs-directory)
  "Directory for variable files used by N1 Emacs, like history, etc.")

(defvar n1-config-lib-directory
  (expand-file-name "lib/" user-emacs-directory)
  "Directory for local elisp code used by N1 Emacs, like history, etc.")

(unless (file-directory-p n1-config-module-directory)
  (mkdir n1-config-module-directory))

(unless (file-directory-p n1-config-var-directory)
  (mkdir n1-config-var-directory))

(unless (file-directory-p n1-config-lib-directory)
  (mkdir n1-config-lib-directory))

(defun n1-local-lib (dirname)
  "Add directory DIRNAME to the load path of Emacs.
DIRNAME must be a relative name as it is expected to reside in the
directory specified by N1-CONFIG-LIB-DIRECTORY."
  (let ((dir (expand-file-name (file-name-as-directory dirname)
			       n1-config-lib-directory)))
    (when (file-directory-p dir)
      (add-to-list 'load-path dir)
      t)))

;; Add the modules folder to the load path
(add-to-list 'load-path n1-config-module-directory)

;;; Load modules

(require 'n1-defaults)
(require 'n1-ui)
(require 'n1-completion)
(require 'n1-misc)
(require 'n1-eglot)
(require 'n1-pass)
;;(require 'n1-speedbar)
;;(require 'n1-tex)
(require 'n1-org)
(require 'n1-denote) ; must be loaded before hyperbole
(require 'n1-hyperbole)

(require 'n1-lisp)
(require 'n1-clojure)
(require 'n1-perl)
;;(require 'n1-haskell)
;;(require 'n1-ocaml)
(require 'n1-raku)
(require 'n1-webdev)

;;; n1-config.el ends here
