;;; n1-org.el --- Org Mode Configuration  -*- lexical-binding: t; -*-

;; Copyright (C) 2023
;; SPDX-License-Identifier: MIT

;; Author: System Crafters Community

;; Commentary

;; Provides basic configuration for Org Mode.

;;; Code:

(n1-package-install-package 'org)
(n1-package-install-package 'org-appear)

;; Set diary file
(setq diary-file "~/Documents/diary")

(require 'org)

;; Return or left-click with mouse follows link
(customize-set-variable 'org-return-follows-link t)
(customize-set-variable 'org-mouse-1-follows-link t)

;; Display links as the description provided
(customize-set-variable 'org-link-descriptive t)

;; Hide markup markers
(customize-set-variable 'org-hide-emphasis-markers t)
(add-hook 'org-mode-hook 'org-appear-mode)

(add-hook 'org-mode-hook 'auto-fill-mode)
;; ;; disable auto-pairing of "<" in org-mode
;; (add-hook 'org-mode-hook (lambda ()
;;     (setq-local electric-pair-inhibit-predicate
;;     `(lambda (c)
;;         (if (char-equal c ?<) t (,electric-pair-inhibit-predicate c))))
;;     (auto-fill-mode t)))

(setq org-log-into-drawer "LOGBOOK")

;; custom todo keywords
;; see also n1-ui.el for custom colors for these keywords
(setq org-todo-keywords
      '((sequence "REPEAT(r)" "NEXT(n)" "TODO(t)" "WAITING(w)" "SOMEDAY(s)" "PROJECT(p)" "|" "DONE(d@)" "CANCELED(c@)")))

(setq org-log-reschedule 'time)

;; Custom Org Tags
(setq org-tag-alist '(("CAR" . ?c)
		      ("WORK" . ?w)))

;; Keybindings
(keymap-global-set "<f9>" 'org-agenda)

(provide 'n1-org)
;;; n1-org.el ends here
