;;;; n1-hyperbole.el --- Hyperbole configuration      -*- lexical-binding: t; -*-

;; Copyright (C) 2024
;; SPDX-License-Identifier: MIT

;; Author: n1to

;;; Commentary:

;; 

;;; Code:

(n1-package-install-package 'hyperbole)

(require 'hyperbole)

(setq hyrolo-file-list '("~/Documents/Hyperb/rolo.org"))
(add-hook 'hyperbole-init-hook
	  (lambda ()
	    (require 'denote)
	    (setq hyrolo-file-list (append hyrolo-file-list
                                           (list denote-directory)))))

(hyperbole-mode 1)
(keymap-global-set "M-RET" 'hkey-either)

(setq hyrolo-date-format "%Y-%m-%d")

(provide 'n1-hyperbole)
;;; n1-hyperbol.el ends here
