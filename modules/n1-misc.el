;;;; n1-misc.el --- Miscellanous Modules             -*- lexical-binding: t; -*-

;; Copyright (C) 2024
;; SPDX-License-Identifier: MIT

;; Author: n1to

;;; Commentary:

;;

;;; Code:

(n1-package-install-package 'magit)
(n1-package-install-package 'csv-mode)


(provide 'n1-misc)
;;; n1-misc.el ends here
