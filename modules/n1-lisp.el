;;; n1-lisp.el --- Lisp Language Support  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  n1to

;; Author: n1to

;;; Commentary:

;; This module provides support for all kinds of Lisp development.

;;; Code:

(require 'eldoc)
;; keeps code indented even when copy/pasting.
(n1-package-install-package 'aggressive-indent)

(n1-package-install-package 'rainbow-delimiters)
(n1-package-install-package 'sly)
(n1-package-install-package 'sly-asdf)
(n1-package-install-package 'sly-quicklisp)
(n1-package-install-package 'sly-repl-ansi-color)
(n1-package-install-package 'racket-mode)
(n1-package-install-package 'paredit)

(add-hook 'emacs-lisp-mode-hook 'electric-pair-mode)
(add-hook 'common-lisp-mode-hook 'electric-pair-mode)
(add-hook 'lisp-mode-hook 'electric-pair-mode)
(add-hook 'scheme-mode-hook 'electric-pair-mode)
;; (add-hook 'clojure-mode-hook 'electric-pair-mode)

(require 'rainbow-delimiters)

(add-hook 'emacs-lisp-mode-hook 'rainbow-delimiters-mode)
(add-hook 'common-lisp-mode-hook 'rainbow-delimiters-mode)
(add-hook 'lisp-mode-hook 'rainbow-delimiters-mode)
(add-hook 'scheme-mode-hook 'rainbow-delimiters-mode)

(add-hook 'emacs-lisp-mode-hook 'paredit-mode)
(add-hook 'common-lisp-mode-hook 'paredit-mode)
(add-hook 'lisp-mode-hook 'paredit-mode)
(add-hook 'scheme-mode-hook 'paredit-mode)
(add-hook 'racket-mode-hook 'paredit-mode)

(when (package-installed-p 'clojure-mode)
    (add-hook 'clojure-mode-hook 'rainbow-delimiters-mode)
    (add-hook 'clojure-mode-hook 'paredit-mode))


(with-eval-after-load 'paredit
  (define-key paredit-mode-map (kbd "M-l") 'paredit-splice-sexp-killing-backward)
  (define-key paredit-mode-map (kbd "M-a") 'paredit-splice-sexp-killing-forward))

(with-eval-after-load 'flycheck
  (add-hook 'emacs-lisp-mode-hook #'flycheck-mode))

;; Common Lisp

(with-eval-after-load 'sly
  (require 'sly-quicklisp)
  (require 'sly-repl-ansi-color)
  (require 'sly-asdf)

  (setq inferior-lisp-program "ros -Q run")
  (setq sly-lisp-implementations
	'((clisp ("clisp" "--quiet"))
          (sbcl ("/usr/bin/sbcl") :env ("SBCL_HOME=/usr/lib64/sbcl"))
          (roswell ("ros" "-Q" "run"))))
  (setq sly-default-lisp 'sbcl)
  (when (file-exists-p "~/.roswell/helper.el")
    (load (expand-file-name "~/.roswell/helper.el"))) ; Load Roswell

  (define-key sly-prefix-map (kbd "C-e") 'sly-eval-last-expression)
  (define-key sly-prefix-map (kbd "C-f") 'sly-eval-defun)
  (define-key sly-prefix-map (kbd "C-q") 'sly-quit-lisp))

(add-hook 'lisp-mode-hook #'sly-editing-mode)
(add-hook 'lisp-mode-hook #'aggressive-indent-mode)

;; Scheme Mode

;; Indenting module body code at column 0
(defun scheme-module-indent (state indent-point normal-indent)
      "Set indentation for any STATE and any INDENT-POINT and NORMAL-INDENT to 0."
      0)
(put 'module 'scheme-indent-function 'scheme-module-indent)
(put 'and-let* 'scheme-indent-function 1)
(put 'parameterize 'scheme-indent-function 1)
(put 'handle-exceptions 'scheme-indent-function 1)
(put 'when 'scheme-indent-function 1)
(put 'unless 'scheme-indent-function 1)
(put 'match 'scheme-indent-function 1)

;; Racket
(setq racket-program "/home/n1to/apps/racket-8.9/bin/racket")

;; Picolisp (https://github.com/tj64/picolisp-mode/)
;; This mode is not in Melpa, etc, but installed locally
;;(n1-local-lib "picolisp-mode")
(when (n1-local-lib "picolisp-mode")
  (require 'picolisp)
  (add-to-list 'auto-mode-alist '("\\.l$" . picolisp-mode))
  (add-hook 'picolisp-mode-hook
	    (lambda ()
              (tsm-mode) ;; Enables TSM
              (define-key picolisp-mode-map (kbd "RET") 'newline-and-indent))))

(provide 'n1-lisp)
;;; n1-lisp.el ends here
