;;; n1-eglot.el --- Eglot configuration              -*- lexical-binding: t; -*-

;; Copyright (C) 2023

;; Author:  n1to
;; Keywords: tools, languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Eglot configuration.

;; Suggested additional keybindings
;; (with-eval-after-load "prog-mode"
;;   (define-key prog-mode-map (kbd "C-c e n") #'flymake-goto-next-error)
;;   (define-key prog-mode-map (kbd "C-c e p") #'flymake-goto-prev-error))

;;; Code:


;; Install dependencies
(n1-package-install-package 'eglot)
;; (n1-package-install-package 'consult-eglot) ; TODO: seems really nice

;;; hooks
(defun crafted-ide--add-eglot-hooks (mode-list)
  "Iterates over MODE-LIST recursively to add eglot-ensure to
existing mode hooks.

The mode must be loaded, ie. found with `fboundp'. A mode which
is not loaded will not have a hook added, in which case add it
manually with something like this:

`(add-hook 'some-mode-hook #'eglot-ensure)'
"
  (dolist (mode-def mode-list)
    (let ((mode (if (listp mode-def) (car mode-def) mode-def)))
      (cond
       ((listp mode) (crafted-ide--add-eglot-hooks mode))
       (t
        (when (and (fboundp mode)
                   (not (eq 'clojure-mode mode))  ; prefer cider
                   (not (eq 'lisp-mode mode))     ; prefer sly/slime
                   (not (eq 'scheme-mode mode))   ; prefer geiser
                   )
          (let ((hook-name (concat (symbol-name mode) "-hook")))
            (message (concat "adding eglot to " hook-name))
            (add-hook (intern hook-name) #'eglot-ensure))))))))

;; add eglot to existing programming modes when eglot is loaded.
(with-eval-after-load "eglot"
  (crafted-ide--add-eglot-hooks eglot-server-programs))

(with-eval-after-load "prog-mode"
  (define-key prog-mode-map (kbd "C-c e n") #'flymake-goto-next-error)
  (define-key prog-mode-map (kbd "C-c e p") #'flymake-goto-prev-error))

;;; customization
;; Shutdown server when last managed buffer is killed
(customize-set-variable 'eglot-autoshutdown t)

(provide 'n1-eglot)
;;; n1-eglot.el ends here
