;;; n1-tex.el --- TeX Support -*- lexical-binding: t; -*-

;; Copyright (C) 2023  n1to

;; Author: n1to
;; Keywords: convenience

;;; Commentary:

;; Code

(n1-package-install-package 'auctex)

(setq TeX-auto-save t)
(setq TeX-parse-self t)

;; ##### Don't forget to configure
;; ##### Okular to use emacs in
;; ##### "Configuration/Configure Okular/Editor"
;; ##### => Editor => Emacsclient. (you should see
;; ##### emacsclient -a emacs --no-wait +%l %f
;; ##### in the field "Command".

;; ##### Always ask for the master file
;; ##### when creating a new TeX file.
(setq-default TeX-master nil)

;; ##### Enable synctex correlation. From Okular just press
;; ##### Shift + Left click to go to the good line.
(setq TeX-source-correlate-mode t
      TeX-source-correlate-start-server t)

;; ### Set Okular as the default PDF viewer.
(eval-after-load "tex"
  '(setcar (cdr (assoc 'output-pdf TeX-view-program-selection)) "Okular"))

(add-hook 'LaTeX-mode-hook 'turn-on-reftex)   ; with AUCTeX LaTeX mode
(add-hook 'latex-mode-hook 'turn-on-reftex)   ; with Emacs latex mode

(provide 'n1-tex)
;;; n1-tex.el ends here
