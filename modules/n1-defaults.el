;;; n1-defaults.el --- Sane default configuration    -*- lexical-binding: t; -*-

;; Copyright (C) 2024

;; Author:  n1to
;; Keywords: internal

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Common settings and defaults.

;;; Code:

(n1-package-install-package 'yasnippet)
(n1-package-install-package 'yasnippet-snippets)
(n1-package-install-package 'which-key)

;; Add ~/bin to search path for executables
(when (file-directory-p "~/bin/")
  (add-to-list 'exec-path "~/bin/"))

;; Custom Backup Directory and Backup File Handling
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))
(message "Deleting old backup files...")
(let ((week (* 60 60 24 7))
      (current (float-time (current-time))))
  (dolist (file (directory-files temporary-file-directory t))
    (when (and (backup-file-name-p file)
               (> (- current (float-time (nth 5 (file-attributes file))))
                  week))
      (message "%s" file)
      (delete-file file))))

;; custom Basic Settings
(setq-default fill-column 80)

;; show line and columns numbers in modeline
(line-number-mode 1)
(column-number-mode 1)

;; custom key bindings
(define-key global-map (kbd "C-ä") 'undo)

;; C source code style
(setq c-default-style '((c-mode . "bsd")
                        (java-mode . "java")
                        (awk-mode . "awk")
                        (other . "gnu")))

;; global bindings for compile commands
(keymap-global-set "<f6>" 'compile)
(keymap-global-set "<f7>" 'recompile)

;; enable bracket autoinsertion, etc. for prog-modes
(add-hook 'prog-mode-hook #'electric-pair-mode)

;; shows a pop-up of available keybindings when typing a sequence
(which-key-mode 1)

;; add local yasnippet directory
(require 'yasnippet)
(let ((snippet-dir (expand-file-name (file-name-as-directory "snippets")
				     user-emacs-directory)))
  (unless (file-directory-p snippet-dir)
    (mkdir snippet-dir))
  (add-to-list 'yas-snippet-dirs snippet-dir t)
  (yas--load-snippet-dirs))

;; enable yasnippet for the following mode hooks
(dolist (mode '(sh-mode-hook))
  (add-hook mode #'yas-minor-mode))

;; ediff configuration
(setq ediff-keep-variants nil)
;; (setq ediff-make-buffers-readonly-at-startup nil)
(setq ediff-window-setup-function 'ediff-setup-windows-plain)
(setq ediff-split-window-function 'split-window-horizontally)

;; Revert Dired and other buffers
(customize-set-variable 'global-auto-revert-non-file-buffers t)

;; Revert buffers when the underlying file has changed
(setopt auto-revert-avoid-polling t)
(setopt auto-revert-interval 5)
(setopt auto-revert-check-vc-info t)
(global-auto-revert-mode 1)

;; Typed text replaces the selection if the selection is active,
;; pressing delete or backspace deletes the selection.
(delete-selection-mode)

;; Use tabs instead of spaces
(setq-default indent-tabs-mode t)

;; Use "y" and "n" to confirm/negate prompt instead of "yes" and "no"
;; Using `advice' here to make it easy to reverse in custom
;; configurations with `(advice-remove 'yes-or-no-p #'y-or-n-p)'
;;
;; N.B. Emacs 28 has a variable for using short answers, which should
;; be preferred if using that version or higher.
(if (boundp 'use-short-answers)
    (setq use-short-answers t)
  (advice-add 'yes-or-no-p :override #'y-or-n-p))

;; Turn on recentf mode
(add-hook 'after-init-hook #'recentf-mode)
(customize-set-variable 'recentf-save-file
                        (expand-file-name "recentf" n1-config-var-directory))

;; Do not save duplicates in kill-ring
(customize-set-variable 'kill-do-not-save-duplicates t)

;; Make scrolling less stuttered
(setq auto-window-vscroll nil)
(customize-set-variable 'fast-but-imprecise-scrolling t)
(customize-set-variable 'scroll-conservatively 101)
(customize-set-variable 'scroll-margin 0)
(customize-set-variable 'scroll-preserve-screen-position t)

;; Better support for files with long lines
(setq-default bidi-paragraph-direction 'left-to-right)
(setq-default bidi-inhibit-bpa t)
(global-so-long-mode 1)

;; Make shebang (#!) file executable when saved
(add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p)

;; Enable savehist-mode for command history
(savehist-mode 1)

;; Keep state files in `crafted-config-var-directory' by default
;; we use `with-eval-after-load' to only affect what is being used.
;;
;; Note that this can introduce issues depending on how each module
;; works. Like, for example, if the module reads those files during
;; load it may happen that it reads the file on its default location
;; before the path is changed (because this code runs after-load,
;; and user customization is run after all of crafted-emacs is loaded)
;;
;; So, each variable needs some thought on how/when to set it,
;; while also trying to not set variables for modules the user
;; is not loading / using.

(defun n1-defaults--sensible-path
    (root varname name)
  "Set the VARNAME to a path named NAME inside ROOT.

  For example (n1-config-var-directory 'savehist-file \"history\")
  Will set `savehist-file' to, ie, ~/.config/emacs/var/history"
  (customize-set-variable varname (expand-file-name name root)))

(n1-defaults--sensible-path n1-config-var-directory
                                 'savehist-file "history")

(n1-defaults--sensible-path n1-config-var-directory
                                 'auto-save-list-file-prefix "auto-save-list/.saves-")

(with-eval-after-load 'saveplace
  (n1-defaults--sensible-path n1-config-var-directory
                                    'save-place-file "places"))

(with-eval-after-load 'bookmark
  (n1-defaults--sensible-path n1-config-var-directory
                                    'bookmark-default-file "bookmarks"))

(with-eval-after-load 'tramp
  (n1-defaults--sensible-path n1-config-var-directory
                                    'tramp-persistency-file-name
                                    "tramp"))

(with-eval-after-load 'org-id
  (n1-defaults--sensible-path n1-config-var-directory
                                    'org-id-locations-file
                                    "org-id-locations"))

(with-eval-after-load 'nsm
  (n1-defaults--sensible-path n1-config-var-directory
                                    'nsm-settings-file
                                    "network-security.data"))

(with-eval-after-load 'project
  (n1-defaults--sensible-path n1-config-var-directory
                                    'project-list-file
                                    "projects"))

;; Regexp Builder syntax
(setf reb-re-syntax 'string)

;; enable repeat-mode
(repeat-mode t)

(provide 'n1-defaults)
;;; n1-defaults.el ends here
