;;; n1-webdev.el --- Web Development Support  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  n1to

;; Author: n1to

;;; Commentary:

;; This module provides support for web development.

;;; Code:

(n1-package-install-package 'web-mode)
(n1-package-install-package 'php-mode)
(n1-package-install-package 'flycheck)
(n1-package-install-package 'flycheck-phpstan)
(n1-package-install-package 'composer)

(require 'web-mode)
(require 'php-mode)
(require 'flycheck)
(require 'flycheck-phpstan)

(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.blade\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html.ep\\'" . web-mode))

(setq web-mode-engines-alist
      '(("php" . "\\.phtml\\'")
        ("blade" . "\\.blade\\.")
        ("blade" . "\\.blade\\.php\\'")))

(setq web-mode-enable-auto-pairing t)
(setq web-mode-enable-css-colorization t)
(setq web-mode-enable-heredoc-fontification t)
(setq web-mode-enable-current-element-highlight t)

(defun n1--php-mode-setup ()
  "My PHP-mode hook."
  (flycheck-mode t)
  (yas-minor-mode 1)
  (subword-mode 1))

(defun n1--web-mode-setup ()
  "My PHP-mode hook."
  (yas-minor-mode 1)
  (subword-mode 1)
  (electric-pair-mode 0))

(add-hook 'web-mode-hook 'n1--web-mode-setup)
(add-hook 'php-mode-hook 'n1--php-mode-setup)

(provide 'n1-webdev)
;;; n1-webdev.el ends here
