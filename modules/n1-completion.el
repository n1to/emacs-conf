;;; n1-completion.el --- Completion configuration    -*- lexical-binding: t; -*-

;; Copyright (C) 2023

;; Author:  n1to
;; Keywords: internal

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(n1-package-install-package 'cape)
(n1-package-install-package 'consult)
(n1-package-install-package 'corfu)
(n1-package-install-package 'corfu-terminal)
(n1-package-install-package 'embark)
(n1-package-install-package 'embark-consult)
(n1-package-install-package 'marginalia)
(n1-package-install-package 'orderless)
(n1-package-install-package 'vertico)

(defun crafted-completion/minibuffer-backward-kill (arg)
  "Delete word or delete up to parent folder when completion is a file.

ARG is the thing being completed in the minibuffer."
  (interactive "p")
  (if minibuffer-completing-file-name
      ;; Borrowed from https://github.com/raxod502/selectrum/issues/498#issuecomment-803283608
      (if (string-match-p "/." (minibuffer-contents))
          (zap-up-to-char (- arg) ?/)
        (delete-minibuffer-contents))
    (backward-kill-word arg)))

;;; Minibuffer completion settings
(setopt enable-recursive-minibuffers t)
(setopt completion-cycle-threshold 1)
(setopt completions-detailed t)
(setopt tab-always-indent 'complete)
(setopt completion-styles '(basic initials substring))

(setopt completion-auto-help 'always)
(setopt completion-max-height 20)
(setopt completions-format 'one-column)
(setopt completions-group t)
(setopt completion-auto-select 'second-tab)

;;; Vertico

(require 'vertico)
(require 'vertico-directory)

;; Cycle back to top/bottom result when the edge is reached
(customize-set-variable 'vertico-cycle t)
(vertico-mode 1)

(keymap-set vertico-map "M-DEL" 'vertico-directory-delete-word)

;;; Consult

(setq consult-narrow-key "<")

(let ((bindings '(("C-x b" . consult-buffer)
		  ("C-s" . consult-line)
		  ("M-y" . consult-yank-pop))))
  (dolist (bnd bindings)
    (keymap-global-set (car bnd) (cdr bnd))))

;;; Marginalia

(require 'marginalia)

(customize-set-variable 'marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
(marginalia-mode 1)

(setq completion-in-region-function #'consult-completion-in-region)

;;; Orderless

;; Set up Orderless for better fuzzy matching
(require 'orderless)
(customize-set-variable 'completion-styles '(orderless basic))
(customize-set-variable 'completion-category-overrides '((file (styles . (partial-completion)))))

;;; Embark
(require 'embark)
(require 'embark-consult)

(global-set-key [remap describe-bindings] #'embark-bindings)
(global-set-key (kbd "C-.") 'embark-act)

;; Use Embark to show bindings in a key prefix with `C-h`
(setq prefix-help-command #'embark-prefix-help-command)

(with-eval-after-load 'embark-consult
  (add-hook 'embark-collect-mode-hook #'consult-preview-at-point-mode))

;;; Corfu

(require 'corfu-popupinfo)
(require 'corfu)

(unless (display-graphic-p)
  (require 'corfu-terminal)
  (corfu-terminal-mode 1))

(customize-set-variable 'corfu-popupinfo-delay '(0.25 . 0.1))
(customize-set-variable 'corfu-popupinfo-hide nil)
(add-hook 'corfu-mode-hook 'corfu-popupinfo-mode)

;; Setup corfu for popup like completion
(customize-set-variable 'corfu-cycle t) ; Allows cycling through candidates
(customize-set-variable 'corfu-auto t)  ; Enable auto completion
(customize-set-variable 'corfu-auto-prefix 2) ; Complete with less prefix keys
(customize-set-variable 'corfu-auto-delay 0.0) ; No delay for completion
(customize-set-variable 'corfu-echo-documentation 0.25) ; Echo docs for current completion option

(global-corfu-mode 1)

(eldoc-add-command #'corfu-insert)

(keymap-set corfu-map "SPC" 'corfu-insert-separator)
(keymap-set corfu-map "C-n" 'corfu-next)
(keymap-set corfu-map "C-p" 'corfu-previous)
(keymap-set corfu-map "M-g" 'corfu-doc-scroll-down)
(keymap-set corfu-map "M-r" 'corfu-doc-scroll-up)
(keymap-set corfu-map "M-h" 'corfu-doc-toggle)
(keymap-set corfu-map "M-d" 'corfu-quit)

;;; Cape

;; Setup Cape for better completion-at-point support and more
(require 'cape)

;; Add useful defaults completion sources from cape
(add-to-list 'completion-at-point-functions #'cape-file)
(add-to-list 'completion-at-point-functions #'cape-dabbrev)

;; Silence the pcomplete capf, no errors or messages!
;; Important for corfu
(advice-add 'pcomplete-completions-at-point :around #'cape-wrap-silent)

;; Ensure that pcomplete does not write to the buffer
;; and behaves as a pure `completion-at-point-function'.
(advice-add 'pcomplete-completions-at-point :around #'cape-wrap-purify)
(add-hook 'eshell-mode-hook
          (lambda () (setq-local corfu-quit-at-boundary t
                            corfu-quit-no-match t
                            corfu-auto nil)
            (corfu-mode)))

(provide 'n1-completion)
;;; n1-completion.el ends here
