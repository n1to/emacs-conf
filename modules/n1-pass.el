;;; n1-pass.el --- Emacs Password Store -*- lexical-binding: t; -*-

;; Copyright (C) 2023  n1to

;; Author: n1to
;; Keywords: convenience

;;; Commentary:

;; Code

(n1-package-install-package 'password-store)

(require 'password-store)
(require 'auth-source)
(require 'auth-source-pass)

(setq auth-sources '(password-store))
(setq epg-gpg-program "gpg2")
(setenv "GPG_AGENT_INFO" nil)

(setq password-store-password-length 32)

(provide 'n1-pass)
;;; n1-pass.el ends here
