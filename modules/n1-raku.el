;;; n1-raku.el --- Raku Language Support  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  n1to

;; Author: n1to

;;; Commentary:

;; This module provides support for all kinds of Raku development.

;;; Code:

(n1-package-install-package 'raku-mode)
(n1-package-install-package 'flymake-rakudo)

(defun n1--raku-mode-init ()
  (flymake-rakudo-setup)
  (flymake-mode 1))

(add-hook 'raku-mode-hook 'n1--raku-mode-init)

(with-eval-after-load "raku-mode"
  (define-key raku-mode-map (kbd "C-c e n") #'flymake-goto-next-error)
  (define-key raku-mode-map (kbd "C-c e p") #'flymake-goto-prev-error))

;; add rakubrew paths
(when (and (file-directory-p "~/.rakubrew/bin/")
	   (file-exists-p "~/.rakubrew/bin/rakubrew"))
  (add-to-list 'exec-path "~/.rakubrew/bin/")
  ;; add rakubrew env dirs to exec path
  (let* ((raw (shell-command-to-string "~/.rakubrew/bin/rakubrew init bash"))
	 (no-prefix (seq-drop (seq-drop-while (lambda (c) (not (char-equal c ?\"))) raw) 1))
	 (path (seq-take-while (lambda (c) (not (char-equal c ?\"))) no-prefix))
	 (paths (seq-filter (lambda (s) (string-prefix-p "/home/n1to/.rakubrew" s)) (split-string path ":"))))
    (dolist (dir paths)
      (add-to-list 'exec-path dir))))

(provide 'n1-raku)
;;; n1-raku.el ends here
